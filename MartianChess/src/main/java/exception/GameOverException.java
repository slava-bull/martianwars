package exception;

import entity.Winner;
import implementation.Chess;

public class GameOverException extends Exception {
    private final Winner winner;
    private final transient Chess chess;

    public GameOverException(Winner winner, Chess chess) {
        this.winner = winner;
        this.chess = chess;
    }

    public Winner getWinner() {
        return winner;
    }

    public Chess getChess() {
        return chess;
    }
}
