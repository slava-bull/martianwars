package martian_chess;

import entity.Coord;
import entity.Figure;
import entity.Move;
import entity.Player;

import java.util.ArrayList;
import java.util.Map;

class MoveVerification {
    private final Board board;
    private Move move;
    private ArrayList<Coord> passedCoords;

    MoveVerification(Board board) {
        this.board = board;
    }

    boolean canMove(Move move) {
        Player player = board.getData().getMoveColor();
        return canMove(move, player);
    }

    private boolean canMove(Move move, Player player) {
        this.move = move;
        return canMoveFrom(player) && canMoveTo(player) && canFigureMove();
    }

    private boolean canMoveTo(Player player) {
        return move.getTo().isValid() && board.getFigureAt(move.getTo()).getColor() != player;
    }

    private boolean canMoveFrom(Player player) {
        return move.getFrom().isValid() &&
                move.getFigure() == board.getFigureAt(move.getFrom()) &&
                move.getFigure().getColor() == player;
    }

    private boolean canFigureMove() {
        this.passedCoords = new ArrayList<>();

        switch (move.getFigure()) {
            case WHITE_WARRIOR:
            case BLACK_WARRIOR:
                return partOfMove(true, false, false, 2, move.getFrom());
            case WHITE_PADWAR:
            case BLACK_PADWAR:
                return partOfMove(false, true, false, 2, move.getFrom());
            case WHITE_DWARF:
            case BLACK_DWARF:
                return partOfMove(true, false, false, 3, move.getFrom());
            case WHITE_AIRMAN:
            case BLACK_AIRMAN:
                return partOfMove(false, true, true, 3, move.getFrom());
            case WHITE_LEADER:
            case BLACK_LEADER:
                return partOfMove(true, true, false, 3, move.getFrom());
            case WHITE_QUEEN:
            case BLACK_QUEEN:
                return partOfMove(true, true, true, 3, move.getFrom()) &&
                        (board.getFigureAt(move.getTo()) == Figure.NONE) &&
                        !squareUnderAttack(move.getTo());
            case WHITE_TOT:
            case BLACK_TOT:
                return canTotMove(move.getFrom());
            case WHITE_PANTAN:
            case BLACK_PANTAN:
                return (move.getFigure().getColor() == Player.WHITE ? signY(move) >= 0 : signY(move) <= 0) && absDeltaX(move) <= 1 && absDeltaY(move) <= 1;
            default:
                return false;
        }
    }

    //обработка ходов всех фигур кроме Tot и Pantan
    private boolean partOfMove(boolean straight, boolean diagonal, boolean fly, int deep, Coord start) {
        return partOfMove(straight, diagonal, fly, deep, start, 0);
    }

    private boolean partOfMove(boolean straight, boolean diagonal, boolean fly, int deep, Coord start, int currentDeep) {
        if (!start.isValid()) return false;

        if (passedCoords.indexOf(start) != -1) return false;

        if (deep == currentDeep) {
            return start.equals(move.getTo());
        }

        if (!fly) {
            if (board.getFigureAt(start) != Figure.NONE && start != move.getFrom()) return false;
        }

        passedCoords.add(start);
        if (straight) {
            if (partOfMove(straight, diagonal, fly, deep, new Coord(start.getX() + 1, start.getY()), currentDeep + 1))
                return true;
            if (partOfMove(straight, diagonal, fly, deep, new Coord(start.getX() - 1, start.getY()), currentDeep + 1))
                return true;
            if (partOfMove(straight, diagonal, fly, deep, new Coord(start.getX(), start.getY() + 1), currentDeep + 1))
                return true;
            if (partOfMove(straight, diagonal, fly, deep, new Coord(start.getX(), start.getY() - 1), currentDeep + 1))
                return true;
        }

        if (diagonal) {
            if (partOfMove(straight, diagonal, fly, deep, new Coord(start.getX() + 1, start.getY() + 1), currentDeep + 1))
                return true;
            if (partOfMove(straight, diagonal, fly, deep, new Coord(start.getX() - 1, start.getY() + 1), currentDeep + 1))
                return true;
            if (partOfMove(straight, diagonal, fly, deep, new Coord(start.getX() + 1, start.getY() - 1), currentDeep + 1))
                return true;
            if (partOfMove(straight, diagonal, fly, deep, new Coord(start.getX() - 1, start.getY() - 1), currentDeep + 1))
                return true;
        }

        passedCoords.remove(start);
        return false;
    }

    private boolean canTotMove(Coord start) {
        return canTotMove(start, 0);
    }

    private boolean canTotMove(Coord start, int deep) {

        if (!start.isValid()) return false;

        if (deep == 2) {
            return start.equals(move.getTo());
        }

        if (board.getFigureAt(start) != Figure.NONE && !start.equals(move.getFrom())) return false;

        if (deep == 0) {
            if (canTotMove(new Coord(start.getX() + 1, start.getY()), deep + 1)) return true;
            if (canTotMove(new Coord(start.getX() - 1, start.getY()), deep + 1)) return true;
            if (canTotMove(new Coord(start.getX(), start.getY() + 1), deep + 1)) return true;
            if (canTotMove(new Coord(start.getX(), start.getY() - 1), deep + 1)) return true;
        }

        if (deep == 1) {
            if (canTotMove(new Coord(start.getX() + 1, start.getY() + 1), deep + 1)) return true;
            if (canTotMove(new Coord(start.getX() - 1, start.getY() + 1), deep + 1)) return true;
            if (canTotMove(new Coord(start.getX() + 1, start.getY() - 1), deep + 1)) return true;
            if (canTotMove(new Coord(start.getX() - 1, start.getY() - 1), deep + 1)) return true;
        }

        return false;
    }

    private boolean squareUnderAttack(Coord to) {
        for (Map.Entry<Coord, Figure> enemyFigure : board.getAllFiguresByColor(board.getData().getMoveColor().getOtherPlayer()).entrySet()) {
            if (enemyFigure.getValue() == Figure.WHITE_QUEEN || enemyFigure.getValue() == Figure.BLACK_QUEEN)
                continue;//принцессы бить не могут
            Move enemyMove = new Move(enemyFigure.getValue(), enemyFigure.getKey(), to);//ход противника на эту клетку
            if (canMove(enemyMove, board.getData().getMoveColor().getOtherPlayer())) return true;
        }
        return false;
    }

    private float signY(Move move) {
        return Math.signum((float) (move.getTo().getY() - move.getFrom().getY()));
    }

    private int absDeltaX(Move move) {
        return Math.abs(move.getTo().getX() - move.getFrom().getX());
    }

    private int absDeltaY(Move move) {
        return Math.abs(move.getTo().getY() - move.getFrom().getY());
    }
}
