package martian_chess;

import entity.Coord;
import entity.Figure;
import entity.Move;
import entity.Player;
import exception.GameOverException;
import exception.InvalidFenException;
import implementation.Chess;

import java.util.ArrayList;
import java.util.List;

public class MartianChess implements Chess {

    private Board board;
    private MoveVerification verification;

    public MartianChess() {
        String fen = "wpdalqadpw/tfffffffft/10/10/10/10/10/10/TFFFFFFFFT/WPDALQADPW w Qq 0 1";
        try {
            this.board = new Board(fen);
            this.verification = new MoveVerification(this.board);
        } catch (InvalidFenException ignore) {
        }
    }

    public MartianChess(String fen) throws InvalidFenException {
        this.board = new Board(fen);
        this.verification = new MoveVerification(this.board);
    }

    MartianChess(Board board) {
        this.board = board;
        this.verification = new MoveVerification(board);
    }

    @Override
    public Chess move(Move move) throws GameOverException {
        if (!verification.canMove(move)) return this;

        Board nextBoard = board.handleMove(move);
        return new MartianChess(nextBoard);
    }

    @Override
    public Figure getFigureAt(Coord coord) {
        return board.getFigureAt(coord);
    }

    @Override
    public List<Move> getAllValidMoves() {
        List<Move> allMoves = new ArrayList<>();
        board.getAllFiguresByColor(board.getData().getMoveColor()).forEach((k, v) -> {
            for (Coord coord : board.getAllSquares()) {
                Move move = new Move(v, k, coord);
                if (verification.canMove(move)) allMoves.add(move);
            }
        });
        return allMoves;
    }

    @Override
    public List<Move> getAllValidMovesFrom(Coord coord) {
        List<Move> list = getAllValidMoves();
        Figure figure = getFigureAt(coord);
        list.removeIf(move -> !move.getFrom().equals(coord) || !move.getFigure().equals(figure));
        return list;
    }

    @Override
    public String getFen() {
        return board.getFen();
    }

    @Override
    public Player getMoveColor() {
        return board.getData().getMoveColor();
    }
}
