package martian_chess;

import entity.Coord;
import entity.Figure;
import entity.Player;

import java.util.HashMap;
import java.util.Map;

abstract class MutableGameData {
    Map<Coord, Figure> figures = new HashMap<>();
    Player moveColor;
    int moveNumber;
    int lastMoves;
    boolean escape;
    boolean enemyEscape;

    Map<Coord, Figure> getFigures() {
        return figures;
    }

    Player getMoveColor() {
        return moveColor;
    }

    int getMoveNumber() {
        return moveNumber;
    }

    int getLastMoves() {
        return lastMoves;
    }

    boolean isEscape() {
        return escape;
    }

    boolean isEnemyEscape() {
        return enemyEscape;
    }
}
