package martian_chess;

import entity.Coord;
import entity.Figure;
import entity.Player;

class FenGenerator {
    private String fen;
    private MutableGameData data;

    public FenGenerator(MutableGameData data) {
        this.data = data;
        this.fen = generate();
    }

    private String generate() {
        return fenFigures() + " " +
                (data.getMoveColor() == Player.WHITE ? "w" : "b") + " " +
                fenEscape() + " " +
                data.getLastMoves() + " " +
                data.getMoveNumber();
    }

    private String fenFigures() {
        StringBuilder sb = new StringBuilder();

        for (int y = 9; y >= 0; --y) {
            for (int x = 0; x < 10; ++x) {
                Figure figure = data.getFigures().get(new Coord(x, y));
                sb.append(figure == Figure.NONE ? '1' : figure.toChar());
            }
            if (y > 0) sb.append('/');
        }

        String result = sb.toString();
        //кол-во единичек в число (111->3)
        String ten = "1111111111";
        for (int i = 10; i >= 2; --i) result = result.replace(ten.substring(0, i), String.valueOf(i));

        return result;
    }

    private String fenEscape() {
        String escape = "";
        if (data.getMoveColor() == Player.WHITE) {
            if (data.isEscape()) escape += 'Q';
            else escape += '-';
            if (data.isEnemyEscape()) escape += 'q';
            else escape += '-';
        } else {
            if (data.isEnemyEscape()) escape += 'Q';
            else escape += '-';
            if (data.isEscape()) escape += 'q';
            else escape += '-';
        }
        return escape;
    }

    public String getFen() {
        return fen;
    }
}
