package martian_chess;

import entity.*;
import exception.GameOverException;
import exception.InvalidFenException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

class Board {
    private final String fen;
    private final MutableGameData data;

    Board(String fen) throws InvalidFenException {
        this.fen = fen;
        this.data = new FenParser(fen);
    }

    Board handleMove(Move move) throws GameOverException {

        GameData gameData = new GameData(data);
        Winner winner = whoWinAfterMove(gameData, move);

        gameData.setFigureAt(move.getFrom(), Figure.NONE);
        gameData.setFigureAt(move.getTo(), move.getFigure());

        gameData.moveUp();
        gameData.flipMoveColor();


        try {
            if (winner != Winner.NONE)
                throw new GameOverException(winner, new MartianChess(new Board(new FenGenerator(gameData).getFen())));

            return new Board(new FenGenerator(gameData).getFen());
        } catch (GameOverException e) {
            throw e;
        } catch (InvalidFenException ignore) {
        }
        return this;
    }

    private Winner whoWinAfterMove(GameData gameData, Move move) {
        if ((getFigureAt(move.getFrom()) == Figure.WHITE_LEADER || getFigureAt(move.getFrom()) == Figure.BLACK_LEADER) &&
                (getFigureAt(move.getTo()) == Figure.WHITE_LEADER || getFigureAt(move.getTo()) == Figure.BLACK_LEADER) || //если вождь убил вождя
                (getFigureAt(move.getTo()) == Figure.WHITE_QUEEN || getFigureAt(move.getTo()) == Figure.BLACK_QUEEN))     //взята принцесса
            return gameData.getMoveColor() == Player.WHITE ? Winner.WHITE : Winner.BLACK;

        if (!(getFigureAt(move.getFrom()) == Figure.WHITE_LEADER || getFigureAt(move.getFrom()) == Figure.BLACK_LEADER) &&
                (getFigureAt(move.getTo()) == Figure.WHITE_LEADER || getFigureAt(move.getTo()) == Figure.BLACK_LEADER))   //если вождя убил не вождь
            return Winner.RANDOM;

        //вычисление LastMoves//
        AtomicInteger whiteFigures = new AtomicInteger();
        AtomicInteger blackFigures = new AtomicInteger();
        getAllFigures().forEach((k, v) -> {
            Player player = v.getColor();
            if (player != null)
                if (player == Player.WHITE) whiteFigures.getAndIncrement();
                else blackFigures.getAndIncrement();
        });
        if (whiteFigures.get() <= 3 && blackFigures.get() <= 3) gameData.addLastMoves();
        //--------------------//

        if (gameData.getLastMoves() > 10)
            return Winner.RANDOM; //если прошло 10 ходов с <=3 фигурами с каждой стороны

        return Winner.NONE;
    }

    MutableGameData getData() {
        return data;
    }

    Figure getFigureAt(Coord coord) {
        if (coord.isValid()) return data.getFigures().get(coord);
        return Figure.NONE;
    }

    String getFen() {
        return fen;
    }

    Map<Coord, Figure> getAllFigures() {
        Map<Coord, Figure> figures = new HashMap<>();
        data.getFigures().forEach(figures::put);
        return figures;
    }

    Map<Coord, Figure> getAllFiguresByColor(Player player) {
        Map<Coord, Figure> figures = new HashMap<>();
        data.getFigures().forEach((k, v) -> {
            if (v.getColor() == player)
                figures.put(k, v);
        });
        return figures;
    }

    List<Coord> getAllSquares() {
        List<Coord> coords = new ArrayList<>();
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) coords.add(new Coord(i, j));

        return coords;
    }
}
