package martian_chess;

import entity.Coord;
import entity.Figure;
import entity.Player;
import exception.InvalidFenException;

class FenParser extends MutableGameData {

    FenParser(String fen) throws InvalidFenException {
        //"wpdalqadpw/tfffffffft/10/10/10/10/10/10/TFFFFFFFFT/WPDALQADPW w Qq 0 1"
        // 0                                                             1 2  3 4
        String[] parts = fen.split(" ");
        if (parts.length != 5) throw new InvalidFenException();

        initFigures(parts[0]);
        moveColor = parts[1].equals("w") ? Player.WHITE : Player.BLACK;
        escape = (moveColor == Player.WHITE) ? parts[2].charAt(0) == 'Q' : parts[2].charAt(1) == 'q';
        enemyEscape = (moveColor == Player.WHITE) ? parts[2].charAt(1) == 'q' : parts[2].charAt(0) == 'Q';
        lastMoves = Integer.valueOf(parts[3]);
        moveNumber = Integer.valueOf(parts[4]);
    }

    private void initFigures(String part) {
        //10->91->811->7111->61111->511111->4111111->31111111->211111111->1111'1111'11 //10->1111'1111'11
        for (int i = 10; i >= 2; --i) part = part.replace(String.valueOf(i), (i - 1) + "1");

        String[] lines = part.split("/");
        for (int y = 9; y >= 0; --y)
            for (int x = 0; x < 10; ++x)
                figures.put(new Coord(x, y), lines[9 - y].charAt(x) == '1' ? Figure.NONE : Figure.toFigure(lines[9 - y].charAt(x)));
    }
}
