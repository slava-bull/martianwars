package martian_chess;

import entity.Coord;
import entity.Figure;
import entity.Player;

class GameData extends MutableGameData {

    GameData(MutableGameData gameData) {
        this.figures = gameData.getFigures();
        this.moveColor = gameData.getMoveColor();
        this.moveNumber = gameData.getMoveNumber();
        this.lastMoves = gameData.getLastMoves();
        this.escape = gameData.isEscape();
        this.enemyEscape = gameData.isEnemyEscape();
    }

    void setFigureAt(Coord from, Figure figure) {
        if (from.isValid()) figures.put(from, figure);
    }

    void moveUp() {
        moveNumber++;
    }

    void flipMoveColor() {
        if (moveColor == Player.BLACK) moveColor = Player.WHITE;
        else if (moveColor == Player.WHITE) moveColor = Player.BLACK;
    }

    void addLastMoves() {
        lastMoves++;
    }
}
