package implementation;

import entity.Coord;
import entity.Figure;
import entity.Move;
import entity.Player;
import exception.GameOverException;

import java.util.List;

public interface Chess {

    Chess move(Move move) throws GameOverException;

    Figure getFigureAt(Coord coord);

    List<Move> getAllValidMoves();

    List<Move> getAllValidMovesFrom(Coord coord);

    String getFen();

    Player getMoveColor();
}
