package entity;

public enum Figure {
    NONE('-'),
    WHITE_WARRIOR('W'),
    WHITE_PADWAR('P'),
    WHITE_DWARF('D'),
    WHITE_AIRMAN('A'),
    WHITE_LEADER('L'),
    WHITE_QUEEN('Q'),
    WHITE_TOT('T'),
    WHITE_PANTAN('F'),

    BLACK_WARRIOR('w'),
    BLACK_PADWAR('p'),
    BLACK_DWARF('d'),
    BLACK_AIRMAN('a'),
    BLACK_LEADER('l'),
    BLACK_QUEEN('q'),
    BLACK_TOT('t'),
    BLACK_PANTAN('f');


    private char name;

    Figure(char name) {
        this.name = name;
    }

    public char toChar() {
        return name;
    }

    public Player getColor() {
        if (this == NONE) return null;
        return (this == Figure.WHITE_WARRIOR ||
                this == Figure.WHITE_PADWAR ||
                this == Figure.WHITE_DWARF ||
                this == Figure.WHITE_AIRMAN ||
                this == Figure.WHITE_LEADER ||
                this == Figure.WHITE_QUEEN ||
                this == Figure.WHITE_TOT ||
                this == Figure.WHITE_PANTAN) ? Player.WHITE : Player.BLACK;
    }

    public static Figure toFigure(char name) {
        switch (name) {
            case 'W':
                return Figure.WHITE_WARRIOR;
            case 'P':
                return Figure.WHITE_PADWAR;
            case 'D':
                return Figure.WHITE_DWARF;
            case 'A':
                return Figure.WHITE_AIRMAN;
            case 'L':
                return Figure.WHITE_LEADER;
            case 'Q':
                return Figure.WHITE_QUEEN;
            case 'T':
                return Figure.WHITE_TOT;
            case 'F':
                return Figure.WHITE_PANTAN;
            case 'w':
                return Figure.BLACK_WARRIOR;
            case 'p':
                return Figure.BLACK_PADWAR;
            case 'd':
                return Figure.BLACK_DWARF;
            case 'a':
                return Figure.BLACK_AIRMAN;
            case 'l':
                return Figure.BLACK_LEADER;
            case 'q':
                return Figure.BLACK_QUEEN;
            case 't':
                return Figure.BLACK_TOT;
            case 'f':
                return Figure.BLACK_PANTAN;
            default:
                return Figure.NONE;
        }
    }
}
