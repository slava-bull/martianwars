package entity;

public enum Player {
    WHITE, BLACK;

    public Player getOtherPlayer() {
        if (this == WHITE) return BLACK;
        else return WHITE;
    }
}

