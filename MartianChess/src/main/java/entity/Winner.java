package entity;

public enum Winner {
    NONE,
    WHITE,
    BLACK,
    RANDOM
}
