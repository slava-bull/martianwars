package entity;

public class Coord {
    private int x;
    private int y;

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int hashCode() {
        return Integer.valueOf(10 * x + y).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Coord)
            return x == ((Coord) obj).x && y == ((Coord) obj).y;
        return false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isValid() {
        return x >= 0 && x < 10 && y >= 0 && y < 10;
    }
}
