package entity;

public class Move {
    private Figure figure;
    private Coord from;
    private Coord to;

    public Move(Figure figure, Coord from, Coord to) {
        this.figure = figure;
        this.from = from;
        this.to = to;
    }

    public Figure getFigure() {
        return figure;
    }

    public Coord getFrom() {
        return from;
    }

    public Coord getTo() {
        return to;
    }
}
