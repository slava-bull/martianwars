import entity.Coord;
import entity.Figure;
import entity.Move;
import exception.GameOverException;
import exception.InvalidFenException;
import implementation.Chess;
import martian_chess.MartianChess;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ChessTest {

    @Test
    public void createGame() {
        Chess chess = new MartianChess();
        assertEquals("wpdalqadpw/tfffffffft/10/10/10/10/10/10/TFFFFFFFFT/WPDALQADPW w Qq 0 1", chess.getFen());
    }

    @Test(expected = InvalidFenException.class)
    public void createGameExeption() throws InvalidFenException {
        new MartianChess("wpdalqad 0 1");
    }

    @Test
    public void flipMove() throws GameOverException {
        Chess chess = new MartianChess();
        chess = chess.move(new Move(Figure.WHITE_PANTAN, new Coord(3, 1), new Coord(3, 2)));
        assertEquals("wpdalqadpw/tfffffffft/10/10/10/10/10/3F6/TFF1FFFFFT/WPDALQADPW b Qq 0 2", chess.getFen());
    }

    @Test
    public void getFigureAt() {
        Chess chess = new MartianChess();
        assertEquals(Figure.WHITE_PANTAN, chess.getFigureAt(new Coord(1, 1)));
        assertEquals(Figure.WHITE_PADWAR, chess.getFigureAt(new Coord(1, 0)));
        assertEquals(Figure.BLACK_WARRIOR, chess.getFigureAt(new Coord(9, 9)));
    }

    @Test
    public void getAllMoves() {
        Chess chess = new MartianChess();
        int size = chess.getAllValidMovesFrom(new Coord(1, 1)).size();
        assertEquals(3, size);
    }
}
