import entity.Move;
import implementation.Chess;
import martian_chess.MartianChess;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class VerificationTest {
    //LQ
    @Test
    public void F_Figure() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/10/10/10/5F4/10 w Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(5, list.size());
    }

    @Test
    public void f_Figure() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/10/10/10/10/5f4 w Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(0, list.size());
    }

    @Test
    public void T_Figure() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/10/10/10/5T4/10 w Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(10, list.size());
    }

    @Test
    public void W_Figure() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/10/10/10/5W4/10 w Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(7, list.size());
    }

    @Test
    public void P_Figure() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/10/10/10/5P4/10 w Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(5, list.size());
    }
    @Test
    public void D_Figure() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/10/10/10/5D4/10 w Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(13, list.size());
    }

    @Test
    public void A_Figure() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/10/10/10/5A4/10 w Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(12, list.size());
    }

    @Test
    public void Q_Figure() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/5f4/10/10/5Q4/10 w Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(28, list.size());
    }

    @Test
    public void Q_Figure2() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/10/10/10/5Q4/10 w Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(34, list.size());
    }

    @Test
    public void q_Figure() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/5F4/10/10/5q4/10 b Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(31, list.size());
    }

    @Test
    public void q_Figure2() throws Exception {
        Chess chess = new MartianChess("10/10/10/10/10/10/10/10/5q4/10 b Qq 0 1");
        List<Move> list = chess.getAllValidMoves();
        assertEquals(34, list.size());
    }
}
