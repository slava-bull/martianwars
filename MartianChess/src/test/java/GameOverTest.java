import entity.Coord;
import entity.Figure;
import entity.Move;
import entity.Winner;
import exception.GameOverException;
import exception.InvalidFenException;
import implementation.Chess;
import martian_chess.MartianChess;
import org.junit.Test;

import static org.junit.Assert.*;

public class GameOverTest {

    @Test
    public void newChessInException() {
        try {
            Chess chess = new MartianChess("10/10/10/5l4/5F4/10/10/10/10/10 w Qq 0 1");
            chess = chess.move(new Move(Figure.WHITE_PANTAN, new Coord(5, 5), new Coord(5, 6)));
        } catch (GameOverException e) {
            assertEquals(Winner.RANDOM, e.getWinner());
            assertEquals(e.getChess().getFen(), "10/10/10/5F4/10/10/10/10/10/10 b Qq 0 2");
            return;
        } catch (InvalidFenException e) {
            fail();
        }
        fail();
    }

    @Test
    public void F_kill_l() {
        try {
            Chess chess = new MartianChess("10/10/10/5l4/5F4/10/10/10/10/10 w Qq 0 1");
            chess = chess.move(new Move(Figure.WHITE_PANTAN, new Coord(5, 5), new Coord(5, 6)));
        } catch (GameOverException e) {
            assertEquals(Winner.RANDOM, e.getWinner());
            return;
        } catch (InvalidFenException e) {
            fail();
        }
        fail();
    }

    @Test
    public void L_kill_l() {
        try {
            Chess chess = new MartianChess("10/10/10/5l4/5L4/10/10/10/10/10 w Qq 0 1");
            chess = chess.move(new Move(Figure.WHITE_LEADER, new Coord(5, 5), new Coord(5, 6)));
        } catch (GameOverException e) {
            assertEquals(Winner.WHITE, e.getWinner());
            return;
        } catch (InvalidFenException e) {
            fail();
        }
        fail();
    }

    @Test
    public void l_kill_L() {
        try {
            Chess chess = new MartianChess("10/10/10/5L4/5l4/10/10/10/10/10 b Qq 0 1");
            chess = chess.move(new Move(Figure.BLACK_LEADER, new Coord(5, 5), new Coord(5, 6)));
        } catch (GameOverException e) {
            assertEquals(Winner.BLACK, e.getWinner());
            return;
        } catch (InvalidFenException e) {
            fail();
        }
        fail();
    }

    @Test
    public void F_kill_q() {
        try {
            Chess chess = new MartianChess("10/10/10/5q4/5F4/10/10/10/10/10 w Qq 0 1");
            chess = chess.move(new Move(Figure.WHITE_PANTAN, new Coord(5, 5), new Coord(5, 6)));
        } catch (GameOverException e) {
            assertEquals(Winner.WHITE, e.getWinner());
            return;
        } catch (InvalidFenException e) {
            fail();
        }
        fail();
    }

    @Test
    public void f_kill_Q() {
        try {
            Chess chess = new MartianChess("10/10/10/5f4/5Q4/10/10/10/10/10 b Qq 0 1");
            chess = chess.move(new Move(Figure.BLACK_PANTAN, new Coord(5, 6), new Coord(5, 5)));
        } catch (GameOverException e) {
            assertEquals(Winner.BLACK, e.getWinner());
            return;
        } catch (InvalidFenException e) {
            fail();
        }
        fail();
    }

    @Test
    public void draw_when_less_3_figure() {
        try {
            Chess chess = new MartianChess("10/10/10/5f4/5F4/10/10/10/10/10 w Qq 0 1");
            for (int i = 0; i < 20; i++) {
                chess = chess.move(new Move(Figure.WHITE_PANTAN, new Coord(5, 5), new Coord(6, 5)));
                chess = chess.move(new Move(Figure.BLACK_PANTAN, new Coord(5, 6), new Coord(6, 6)));
                chess = chess.move(new Move(Figure.WHITE_PANTAN, new Coord(6, 5), new Coord(5, 5)));
                chess = chess.move(new Move(Figure.BLACK_PANTAN, new Coord(6, 6), new Coord(5, 6)));
            }
        } catch (GameOverException e) {
            assertEquals(Winner.RANDOM, e.getWinner());
            return;
        } catch (InvalidFenException e) {
            fail();
        }
        fail();
    }
}
