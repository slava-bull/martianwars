package app;

import controller.GameController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class App extends Application {
    public static Stage stage;
    @Override
    public void start(Stage primaryStage) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("/PrimaryScene.fxml"));
        primaryStage.setTitle("Martian Wars");
        primaryStage.setScene(new Scene(root, 390, 300));
        primaryStage.setResizable(false);
        stage = primaryStage;
        stage.show();
    }

    @Override
    public void stop() throws IOException {
        if (GameController.chess == null)
            return;
        FileOutputStream fos = new FileOutputStream("fen.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(GameController.chess.getFen());
        oos.flush();
        oos.close();
    }

    public static void main(String[] args) {
        launch(args);
    }
}