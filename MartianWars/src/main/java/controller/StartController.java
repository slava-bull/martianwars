package controller;

import app.App;
import entity.Player;
import implementation.Chess;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import martian_chess.MartianChess;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class StartController {

    @FXML
    Label exceptionLabel = new Label();

    public void onClickNewGame() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/NewGameScene.fxml"));
        App.stage.setScene(new Scene(root));
    }
    public void onClickLoadGame() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("fen.out")))
        {
            String fen = (String) ois.readObject();
            Chess chess = new MartianChess(fen);
            Player player = chess.getMoveColor();
            NewGameController controller = new NewGameController();

            if (player == Player.WHITE) {
                controller.initGame(chess, "White", "Black");
            } else {
                controller.initGame(chess, "Black", "White");
            }
        } catch (Exception e) {
            exceptionLabel.setVisible(true);
        }
    }
}
