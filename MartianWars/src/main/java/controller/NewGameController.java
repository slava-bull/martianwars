package controller;

import app.App;
import entity.Coord;
import entity.Figure;
import implementation.Chess;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import martian_chess.MartianChess;

import java.io.IOException;

public class NewGameController {
    @FXML
    private TextField textFieldFirstPlayer;

    @FXML
    private TextField textFieldSecondPlayer;

    public void onClickBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/PrimaryScene.fxml"));
        App.stage.setScene(new Scene(root));
    }

    public void onClickStart() {
        Chess chess = new MartianChess();
        initGame(chess, textFieldFirstPlayer.getText(), textFieldSecondPlayer.getText());
    }

    private void fillGridPane(GridPane root) {
        for (int i = 0; i < 10; i++) { // заполняем GridPane коллонами и строками
            ColumnConstraints column = new ColumnConstraints();
            RowConstraints row = new RowConstraints();
            column.setPrefWidth(70);
            row.setPrefHeight(70);
            root.getRowConstraints().add(row);
            root.getColumnConstraints().add(column);
        }
    }

    public void initGame(Chess chess, String firstName, String secondName) {
        Label label = new Label("Current player");
        Label currentPlayer = new Label(firstName);
        label.setFont(new Font("Meiryo Bold", 20));
        currentPlayer.setFont(new Font("Meiryo Bold", 20));

        HBox hBox = new HBox(40, label, currentPlayer);
        hBox.setAlignment(Pos.CENTER);

        GameController controller = new GameController(chess, currentPlayer, firstName, secondName);

        GridPane gridPane = new GridPane();
        fillGridPane(gridPane);
        fillPictures(gridPane, chess);
        gridPane.setOnMouseClicked(controller);

        BackgroundImage backgroundImage = new BackgroundImage(new Image("/Back.png"),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        gridPane.setBackground(new Background(backgroundImage));

        VBox root = new VBox(0, hBox, gridPane);
        App.stage.setScene(new Scene(root, 700, 730));
    }

    public static void fillPictures(GridPane root, Chess chess) {
        for (int i = 0; i < 10; i++) { // Заполняем GridPane картинками
            for (int j = 0; j < 10; j++) {
                ImageView imageView = new ImageView();
                Coord coord = new Coord(j, i);
                String path = getPathByFigure(chess.getFigureAt(coord), coord.getX(), coord.getY());
                if (path.equals("")) {
                    imageView.setFitHeight(70);
                    imageView.setFitWidth(70);
                    root.add(imageView, i, j);
                    continue;
                }
                imageView = new ImageView(path);
                imageView.setFitHeight(70);
                imageView.setFitWidth(70);
                root.add(imageView, i, j);
            }
        }
    }
    public static String getPathByFigure(Figure figure, int i, int j) {
        String path = "";
        switch (figure) {
            case NONE:
                break;
            case BLACK_TOT:
                path = "/BlackTot.png";
                break;
            case BLACK_AIRMAN:
                path = "/BlackAirman.png";
                break;
            case BLACK_DWARF:
                path = "/BlackDwarf.png";
                break;
            case BLACK_LEADER:
                path = "/BlackLeader.png";
                break;
            case BLACK_PADWAR:
                path = "/BlackPadwar.png";
                break;
            case BLACK_PANTAN:
                path = "/BlackPantan.png";
                break;
            case BLACK_QUEEN:
                path = "/BlackQueen.png";
                break;
            case BLACK_WARRIOR:
                path = "/BlackWarrior.png";
                break;
            case WHITE_AIRMAN:
                path = "/WhiteAirman.png";
                break;
            case WHITE_DWARF:
                path = "/WhiteDwarf.png";
                break;
            case WHITE_LEADER:
                path = "/WhiteLeader.png";
                break;
            case WHITE_PADWAR:
                path = "/WhitePadwar.png";
                break;
            case WHITE_PANTAN:
                path = "/WhitePantan.png";
                break;
            case WHITE_QUEEN:
                path = "/WhiteQueen.png";
                break;
            case WHITE_TOT:
                path = "/WhiteTot.png";
                break;
            case WHITE_WARRIOR:
                path = "/WhiteWarrior.png";
                break;
        }

        return path;

    }
}
