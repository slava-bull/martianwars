package controller;

import app.App;
import entity.*;
import exception.GameOverException;
import implementation.Chess;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import martian_chess.MartianChess;

import java.util.List;
import java.util.Random;

public class GameController implements EventHandler<MouseEvent> {
    public static Chess chess; // model
    private String nameFirstPlayer;
    private String nameSecondPlayer;
    private Label labelName;
    private boolean firstTap = true;
    private List<Move> possibleMoves;
    private Figure currentFigure;
    private Coord currentCoordFrom;
    private Player player;
    private Label winnerLabel = new Label();

    GameController(Chess chess, Label labelName, String nameFirstPlayer, String nameSecondPlayer) {
        GameController.chess = chess;
        this.labelName = labelName;
        this.nameFirstPlayer = nameFirstPlayer;
        this.nameSecondPlayer = nameSecondPlayer;
        this.player = Player.WHITE;
    }

    public void handle(MouseEvent event) {
        int Y = (int)event.getY()/70;
        int X = (int)event.getX()/70;
        GridPane pane = (GridPane) event.getSource();
        if (firstTap) {
            Coord coordFrom = new Coord(Y, X);
            Figure figure = chess.getFigureAt(coordFrom);
            List<Move> list = chess.getAllValidMovesFrom(coordFrom);

            if (list.size() == 0)
                return;
            for (Move move : list) { // Рисуем на поле ходы
                Coord coordTo = move.getTo();
                ImageView imageView;
                if ((coordTo.getY() + coordTo.getX()) % 2 == 0)
                    imageView = new ImageView("/BlackTo.png");
                else
                    imageView = new ImageView("/WhiteTo.png");
                imageView.setFitHeight(70);
                imageView.setFitWidth(70);
                pane.add(imageView, coordTo.getY(), coordTo.getX());
                NewGameController.fillPictures(pane, chess);
            }
            currentCoordFrom = coordFrom;
            currentFigure = figure;
            possibleMoves = list;
            firstTap = false;
        } else {
            if (checkCoord(Y, X)) { // Проверяем вторую координату
                Move move = new Move(currentFigure, currentCoordFrom, new Coord(Y, X));
                try {
                    chess = chess.move(move);
                    player = player.getOtherPlayer();
                } catch (GameOverException e) {
                    Winner winner = e.getWinner();
                    gameOverHandle(winner);
                }
            }
            firstTap = true;
            changeName();
            pane.getChildren().clear();
            NewGameController.fillPictures(pane, chess);
        }

    }

    private boolean checkCoord(int X, int Y) {
        Coord coordTo;
        for (Move move : possibleMoves) {
            coordTo = move.getTo();
            if (Y == coordTo.getY() && X == coordTo.getX()) {
                return true;
            }
        }
        return false;
    }

    private void changeName() {
        if (player.equals(Player.WHITE)) {
            labelName.setText(nameFirstPlayer);
        } else {
            labelName.setText(nameSecondPlayer);
        }
    }

    private void gameOverHandle(Winner winner) {
        String nameWinner = "";
        switch (winner) {
            case NONE:
                nameWinner = "The winner is not determined";
                break;
            case WHITE:
                nameWinner = nameFirstPlayer;
                break;
            case BLACK:
                nameWinner = nameSecondPlayer;
                break;
            case RANDOM:
                if (new Random().nextBoolean())
                    nameWinner = nameFirstPlayer;
                else
                    nameWinner = nameSecondPlayer;
                break;
        }
        winnerLabel.setText(nameWinner);
        winnerLabel.setFont(new Font("Meiryo Bold", 20));

        Label label1 = new Label("Game Over!");
        label1.setFont(new Font("Meiryo Bold", 27));
        label1.setTextFill(Color.web("#da2727"));
        Label label2 = new Label("Winner");
        label2.setFont(new Font("Meiryo Bold", 23));
        label2.setTextFill(Color.web("#394ea1"));

        VBox root = new VBox(20, label1, label2, winnerLabel);
        root.setAlignment(Pos.CENTER);
        chess = new MartianChess();
        App.stage.setScene(new Scene(root, 300, 250));
    }
}
